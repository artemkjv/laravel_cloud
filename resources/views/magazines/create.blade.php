@extends('layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create Magazine</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route("home") }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route("magazines.index") }}">Magazines</a></li>
                        <li class="breadcrumb-item active">Create Magazine</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <form role="form" enctype="multipart/form-data" method="post" action="{{ route('magazines.store', \request()->query()) }}">
                <div class="row">
                    <div class="col-xl-6 col-sm-12">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name"
                                   class="form-control @error('name') is-invalid @enderror"
                                   id="name"
                                   value="{{ old('name') }}"
                                   placeholder="Name">
                        </div>

                        <div class="form-group">
                            <label for="preview">Preview</label>
                            <input class="form-control @error('preview') is-invalid @enderror" type="file" accept=".jpg, .jpeg, .png" name="preview" id="preview">
                        </div>

                        <div class="form-group">
                            <label for="external_id">External Id</label>
                            <input id="external_id" name="external_id"
                                   class="form-control @error('external_id') is-invalid @enderror" value="{{ old('external_id') }}" type="text">
                        </div>

                    </div>

                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
                <!-- /.row -->
            </form>
        </div><!-- /.container-fluid -->
    </section>
@endsection
