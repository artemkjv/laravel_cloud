@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="row g-3 mt-2">
                <div class="col-6 col-md-3">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" value="{{ \request()->get('name') }}" name="name" id="name">
                </div>
                <div class="col-auto">
                    <label for="" style="visibility: hidden; display: block;">Hidden</label>
                    <button type="submit" class="btn btn-primary mb-3">Submit</button>
                </div>
                <div class="col-auto">
                    <label for="" style="visibility: hidden; display: block;">Hidden</label>
                    <a href="{{ route('magazines.create', \request()->query()) }}" class="btn btn-success mb-3">Create
                        Magazine</a>
                </div>
            </form>


            @if($magazines->isNotEmpty())
                <div class="table-wrapper">
                    <table class="table">
                        <thead class="table-dark">
                        <tr>
                            <th scope="col">Preview</th>
                            <th scope="col">Name</th>
                            <th scope="col">External Id</th>
                            <th scope="col">URL</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($magazines as $magazine)
                            <tr>
                                <td>
                                    @if($magazine->preview)
                                        <img src="/storage/{{ $magazine->preview }}" style="height: 50px" alt="Preview">
                                    @else
                                        No Preview
                                    @endif
                                </td>
                                <td>{{ $magazine->name }}</td>
                                <td>{{ $magazine->external_id }}</td>
                                <td>
                                    <a href="{{ route('magazines.show', ['magazine' => $magazine->id]) }}">{{ route('magazines.show', ['magazine' => $magazine->id]) }}</a>
                                </td>
                                <td>
                                    <div class="d-flex gap-2">
                                        <a class="p-0" class="m-4"
                                           href="{{ route('magazines.edit', ['magazine' => $magazine->id]) }}">
                                            <button class="btn btn-primary">
                                                @svg('bi-pencil')
                                            </button>
                                        </a>
                                        <form method="post"
                                              action="{{ route('magazines.destroy', ['magazine' => $magazine->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">@svg('bi-trash')</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $magazines->links("pagination::bootstrap-5") }}
            @else
                No magazines yet...
            @endif

        </div>
    </div>
@endsection
