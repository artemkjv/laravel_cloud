<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $magazine->name }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <script>
        document.addEventListener('DOMContentLoaded', function(){ // Аналог $(document).ready(function(){
            let css = '.fullscreen-button,.fullscreen-button-mini{display:none!important}'
            let style = document.createElement('style');
            style.type = 'text/css';
            if (style.styleSheet){
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            let frame = document.getElementById('magazine')
            frame.contentWindow.document.head.appendChild(style);
        });
    </script>
</head>
<body>
<div class="position-absolute">
    <iframe id="magazine" name="magazine" type="text/html" scrolling="no" frameborder="0" allowfullscreen="allowfullscreen"
            allow="accelerometer; autoplay; encrypted-media; fullscreen *; gyroscope; picture-in-picture;"
            src="https://online.flippingbook.com/view/{{ $magazine->external_id }}"
            style="width: 100%; height: 100vh;">
    </iframe>
</div>
</body>
</html>
