<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Magazine extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'external_id',
        'preview',
    ];

    public const PAGINATE = 10;

    public static function paginate(?string $name = null) {
        return self::query()
            ->when(!is_null($name), function ($query) use ($name) {
                $query->where('name', 'LIKE', "%$name%");
            })
            ->orderByDesc('id')
            ->paginate(self::PAGINATE);
    }

}
