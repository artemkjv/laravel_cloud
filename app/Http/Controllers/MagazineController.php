<?php

namespace App\Http\Controllers;

use App\Http\Requests\Magazine\StoreRequest;
use App\Http\Requests\Magazine\UpdateRequest;
use App\Models\Magazine;
use App\Services\MagazineService;
use Illuminate\Http\Request;

class MagazineController extends Controller
{

    public function index()
    {
        $magazines = Magazine::paginate(
            \request()->query('name')
        );
        return view('magazines.index', compact('magazines'));
    }

    public function create()
    {
        return view('magazines.create');
    }

    public function store(StoreRequest $request, MagazineService $service)
    {
        $payload = $request->validated();
        $payload['preview'] = $service->handleUploadedPreview($request->file('preview'));
        Magazine::create($payload);
        return to_route('magazines.index');
    }

    public function edit(Magazine $magazine)
    {
        return view('magazines.edit', compact('magazine'));
    }

    public function update(UpdateRequest $request, Magazine $magazine, MagazineService $service)
    {
        $payload = $request->validated();
        if (isset($payload['preview'])) {
            $payload['preview'] = $service->handleUploadedPreview($request->file('preview'));
        }
        $magazine->update($payload);
        return to_route('magazines.index');
    }

    public function destroy(Magazine $magazine)
    {
        $magazine->delete();
        return back();
    }

    public function show(Magazine $magazine) {
        return view('magazines.show', compact('magazine'));
    }

}
