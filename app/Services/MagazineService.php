<?php

namespace App\Services;

class MagazineService
{

    public function handleUploadedPreview($icon){
        if(!is_null($icon)){
            return $icon->store('previews', 'public');
        }
    }

}
